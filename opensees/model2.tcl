# --------------------------------------------------------------------------------------------------
# Model 2
# elasticBeamColumn ELEMENT - 10 elements
# EQ ground motion with gravity
#
# all units are in kip, inch, second
#		
#
#    ^Y
#    |
#    2       __ 
#    |         | 
#    |         | 
#    |         | 
#  (1)      24'
#    |         | 
#    |         | 
#    |         | 
#  =1=    ----  -------->X
#

# SET UP ----------------------------------------------------------------------------
wipe;						       # clear opensees model
model basic -ndm 2 -ndf 3;	       # 2 dimensions, 3 dof per node
file mkdir data; 				   # create data directory

set G 386

# define GEOMETRY -------------------------------------------------------------
# nodal coordinates:
node 1 0. 0.;					   # node#, X Y
node 2 0. 288.
node 3 0. 29.
node 4 0. 58.
node 5 0. 86.
node 6 0. 115.
node 7 0. 144.
node 8 0. 173.
node 9 0. 202.
node 10 0. 230.
node 11 0. 259.

# Single point constraints -- Boundary Conditions
fix 1 1 1 1; 			           # node DX DY RZ

# nodal masses:
mass 2 1.35 1.35 0.;			   # node#, Mx My Mz, Mass=Weight/g.

# Define ELEMENTS -------------------------------------------------------------
# define geometric transformation: performs a linear geometric transformation of beam stiffness and resisting force from the basic system to the global-coordinate system
geomTransf Linear 1;  		       # associate a tag to transformation

# connectivity:
element elasticBeamColumn 1  1  3  1810 3317 97557 1  -mass 0.00043;	# element elasticBeamColumn $eleTag $iNode $jNode $A $E $Iz $transfTag
element elasticBeamColumn 2  3  4  1810 3317 97557 1  -mass 0.00043;	# element elasticBeamColumn $eleTag $iNode $jNode $A $E $Iz $transfTag
element elasticBeamColumn 3  4  5  1810 3317 97557 1  -mass 0.00043;	# element elasticBeamColumn $eleTag $iNode $jNode $A $E $Iz $transfTag
element elasticBeamColumn 4  5  6  1810 3317 97557 1  -mass 0.00043;	# element elasticBeamColumn $eleTag $iNode $jNode $A $E $Iz $transfTag
element elasticBeamColumn 5  6  7  1810 3317 97557 1  -mass 0.00043;	# element elasticBeamColumn $eleTag $iNode $jNode $A $E $Iz $transfTag
element elasticBeamColumn 6  7  8  1810 3317 97557 1  -mass 0.00043;	# element elasticBeamColumn $eleTag $iNode $jNode $A $E $Iz $transfTag
element elasticBeamColumn 7  8  9  1810 3317 97557 1  -mass 0.00043;	# element elasticBeamColumn $eleTag $iNode $jNode $A $E $Iz $transfTag
element elasticBeamColumn 8  9  10 1810 3317 97557 1  -mass 0.00043;	# element elasticBeamColumn $eleTag $iNode $jNode $A $E $Iz $transfTag
element elasticBeamColumn 9  10 11 1810 3317 97557 1  -mass 0.00043;	# element elasticBeamColumn $eleTag $iNode $jNode $A $E $Iz $transfTag
element elasticBeamColumn 10 11 2  1810 3317 97557 1  -mass 0.00043;	# element elasticBeamColumn $eleTag $iNode $jNode $A $E $Iz $transfTag


# define STATIC LOAD-------------------------------------------------------------
timeSeries Linear 1
pattern Plain 1 1 {
   load 2 0. 0. 0.;				# node#, FX FY MZ --  applied static load
}
constraints Plain;     				# how it handles boundary conditions
numberer Plain;					# renumber dof's to minimize band-width (optimization), if you want to
system BandGeneral;				# how to store and solve the system of equations in the analysis
algorithm Linear;         		       	# use Linear algorithm for linear analysis
integrator LoadControl 0.1;			# determine the next time step for an analysis, # apply load in 10 steps
analysis Static					# define type of analysis static or transient
analyze 10;					# perform analysis
loadConst -time 0.0;				# hold load constant and restart time

# Gravity in the vertical direction
timeSeries Path 3 -dt 0.004167 -filePath vert_accel.tcl -factor $G;
pattern UniformExcitation 3 2 -accel 3;

# DYNAMIC ground-motion analysis -------------------------------------------------------------
timeSeries Path 2 -dt 0.004167 -filePath AFSE_ALL_EQ.tcl -factor $G;	# define acceleration vector from file (dt=0.004167 is associated with the input file gm)
pattern UniformExcitation 2 1 -accel 2;		           	# define where and how (pattern tag, dof) acceleration is applied


# Define RECORDERS -------------------------------------------------------------
recorder Node    -file Data/Model2-Results/model2-DFree.out -time -node  2 -dof 1 2 3 disp;			            # displacements of free nodes
recorder Node    -file Data/Model2-Results/model2-RBase.out -time -node  1 -dof 1 2 3 reaction;			        # support reaction
recorder Drift   -file Data/Model2-Results/model2-Drift.out -time -iNode 1 -jNode 2 -dof 1  -perpDirn 2 ;		# lateral drift
recorder Element -file Data/Model2-Results/model2-FCol.out  -time -ele   1 force;			                    # element forces -- column
recorder Node    -file Data/Model2-Results/model2-DCol.out  -time -node 3 -dof 3 disp

# set damping based on first eigen mode
set freq [expr [eigen -fullGenLapack 1]**0.5]
set dampRatio 0.02
rayleigh 0. 0. 0. [expr 2*$dampRatio/$freq]

# display displacement shape of the column
#recorder display "Displaced shape" 10 10 500 500 -wipe
#prp 200. 50. 1;
#vup  0  1 0;
#vpn  0  0 1;
#display 1 5 40 

# create the analysis
wipeAnalysis;					     # clear previously-define analysis parameters
constraints Plain;     				 # how it handles boundary conditions
numberer Plain;					     # renumber dof's to minimize band-width (optimization), if you want to
system BandGeneral;					 # how to store and solve the system of equations in the analysis
algorithm Linear					 # use Linear algorithm for linear analysis
integrator Newmark 0.5 0.25 ;	     # determine the next time step for an analysis
analysis Transient;					 # define type of analysis: time-dependent
analyze 504000 0.004167;					 # apply 504,000 0.04167s time steps in analysis


puts "Done"
wipe


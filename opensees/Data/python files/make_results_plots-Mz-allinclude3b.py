# -*- coding: utf-8 -*-
"""
Created on Thu Sep 21 10:16:08 2017

@author: Wes
"""

#%% Importing libraries
import numpy as np
import matplotlib.pyplot as plt

#%% Constants
earthquakes = [[0,36000],
               [36001,72000],
               [72001,120000],
               [120001,168000],
               [168001,228000],
               [228001,288000],
               [288001,360000],
               [360001,432000],
               [432001,504000]]

models = ['3','3b','4a','4b','4c','4d','5a','5b','5c','5d','Experiment']
model_markers = ['r^','b^','r<','b<','y<','c<','rp','bp','yp','cp','kd']

earthquake_labels = ['EQ1 Loma Prieta \n Agnew State \n Hospital Station ','EQ2 Loma Prieta \n Corralitos Station','EQ3 Loma Prieta \n LGPC Station','EQ4 Loma Prieta \n Corralitos Station','EQ5 Kobe \n Scale: -0.8','EQ6 Loma Prieta \n LGPC Station','EQ7 Kobe \n Scale: 1.0','EQ8 Kobe \n Scale: -1.2','EQ9 Kobe \n Scale: 1.2']
#%%
dresults = list()
for model in models:
    # Reading Reaction results
    fname = 'Model%s-Results/model%s-RBase.out'%(model,model)
    print ('Working in file %s'%(fname))
    data = np.genfromtxt (fname, delimiter="")
    dresult_i = list()
    for earthquake in earthquakes:
        dresult_i.append(max(np.abs(data[earthquake[0]:earthquake[1],3])))
    # Getting maximum value
    dresults.append(dresult_i)

#%% Create plots - x-axis earthquake.  Y-axis Base Moment.  Models with different simbols
xaxis = np.array(range(9))+1
plt.close('all')
for dresult, marker, model in zip(dresults,model_markers, models):
    plt.plot(xaxis, dresult, marker, markersize = 7,label = model)

plt.xlabel ('Earthquake')
plt.ylabel ('Max Base Moment (in-kip)')
plt.grid()
plt.xticks (xaxis,earthquake_labels,rotation='vertical')
plt.xlim([0.5,9.5])
plt.ylim([0,70000])
plt.legend(numpoints = 1, bbox_to_anchor=(1, 1.025), loc=2, title='Model')
plt.tight_layout()
plt.savefig('Base-Moment-omit1and2add3b.pdf', bbox_inches='tight')


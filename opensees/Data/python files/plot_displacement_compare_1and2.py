# -*- coding: utf-8 -*-
 
    
"""
Created on Thu Sep 21 10:16:08 2017

@author: Wes
"""

#%% Importing libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#%% Constants
earthquakes = [[1,0,36000],
               [2,36001,72000],
               [3,72001,120000],
               [4,120001,168000],
               [5,168001,228000],
               [6,228001,288000],
               [7,312001,360000],
               [8,360001,432000],
               [9,432001,504000]]

models = ['1','2']

#%%
for model in models:
    # Reading displacement results
    fname = 'Model%s-Results/model%s-DFree.out'%(model,model)
    print ('Working in file %s'%(fname))
    data = np.genfromtxt (fname, delimiter="")

    plt.plot(data[:,0],data[:,1])
    plt.xlabel('Time (s)')
    plt.ylabel ('Displacement (in)')
    font = {'weight' : 'bold','size' : 26}   
  #  plt.savefig('Displacement_Result_model%s_EQ%s.pdf'%(model,earthquake[0]), bbox_inches='tight')
  #  plt.close("all")

 # -*- coding: utf-8 -*-
# """
# This file plots the reactions of the column
# """
 
#%% Import libraries
import numpy as np
import matplotlib.pyplot as plt
 


fname = 'deflection_vs_number_of_elements.csv'
numelems = np.loadtxt(fname, delimiter = ',')
print ('Working in file ')
plt.close("all")
plt.plot(numelems[:,0],numelems[:,1])
plt.xlabel('Number of Elements')
plt.ylabel ('Maximum Displacement (in)')
plt.xlim([0,15])
plt.ylim([7.6,7.8])
plt.grid()
font = {'weight' : 'bold','size' : 26}   
plt.savefig('number_of_elements_for_model2.pdf', bbox_inches='tight')
plt.close("all")
    


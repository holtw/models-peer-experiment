# -*- coding: utf-8 -*-
 
    
"""
Created on Thu Sep 21 10:16:08 2017

@author: Wes
"""

#%% Importing libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#%% Constants
earthquakes = [[1,0,504000]]

models = ['1']

#%%
for model in models:
    # Reading displacement results
    fname = 'Model%s-Results/model%s-DFree.out'%(model,model)
    print ('Working in file %s'%(fname))
    data = np.genfromtxt (fname, delimiter="")
    for earthquake in earthquakes:
        print ('Working on EQ%s'%(earthquake[0]))
        plt.plot(data[earthquake[1]:earthquake[2],0],data[earthquake[1]:earthquake[2],1])
        plt.xlabel('Time (s)')
        plt.ylabel ('EQ%s Displacement (in)'%(earthquake[0]))
        font = {'weight' : 'bold','size' : 26}   
   #     plt.savefig('Compare_Displacement_model_1and2_EQ%s.pdf'%(earthquake[0]), bbox_inches='tight')
   #     plt.close("all")

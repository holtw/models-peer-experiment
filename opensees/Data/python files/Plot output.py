# -*- coding: utf-8 -*-
"""
Created on Wed Aug 31 14:00:51 2016

@author: Wes
"""
 
import numpy as np
import pandas as pd
 
# Reading and plotting output data from opensees model
numpy.loadtxt('Drift.out')



data = pd.read_csv('Drift.out',parse_dates=True,index_col=0,names = ['time','x'])

data.plot(0)


# -*- coding: utf-8 -*-
"""
Created on Thu Sep 21 10:16:08 2017

@author: Wes
"""

#%% Importing libraries
import numpy as np
import matplotlib.pyplot as plt

#%% Constants
earthquakes = [[0,36000],
               [36001,72000],
               [72001,120000],
               [120001,168000],
               [168001,228000],
               [228001,288000],
               [288001,360000],
               [360001,432000],
               [432001,504000]]

models = ['1','2','3','4a','4b','4c','4d','5a','5b','5c','5d','Experiment']
model_markers = ['ro','rv','r^','r<','b<','y<','c<','rp','bp','yp','cp','kd']
earthquake_ids = ['1','2','3','4','5','6','7','8','9']
earthquake_labels = ['EQ1 Loma Prieta \n Agnew State \n Hospital Station ','EQ2 Loma Prieta \n Corralitos Station','EQ3 Loma Prieta \n LGPC Station','EQ4 Loma Prieta \n Corralitos Station','EQ5 Kobe \n Scale: -0.8','EQ6 Loma Prieta \n LGPC Station','EQ7 Kobe \n Scale: 1.0','EQ8 Kobe \n Scale: -1.2','EQ9 Kobe \n Scale: 1.2']
#%%
dresults = list()
for model in models:
    # Reading displacement results
    fname = 'Model%s-Results/model%s-DFree.out'%(model,model)
    print ('Working in file %s'%(fname))
    datad = np.genfromtxt (fname, delimiter="")
    # Reading moment results
    fname = 'Model%s-Results/model%s-RBase.out'%(model,model)
    datam = np.genfromtxt (fname, delimiter="")
        
    
    for earthquake, earthquake_id in zip(earthquakes, earthquake_ids):
        displacements = datad[earthquake[0]:earthquake[1],1]
        moments = datam[earthquake[0]:earthquake[1],3]

        # Creating the plots
        plt.close('all')
        plt.plot(displacements,moments)
        plt.ylabel ('Moment (in-kips)')
        plt.xlabel ('Displacement (in)')
        plt.grid()
        plt.tight_layout()
        plt.savefig('Moment_displacement_eq%s_m%s.pdf'%(earthquake_id, model), bbox_inches='tight')

        


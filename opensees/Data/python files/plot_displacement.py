# -*- coding: utf-8 -*-
 
    
"""
Created on Thu Sep 21 10:16:08 2017

@author: Wes
"""

#%% Importing libraries
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

#%% Constants
earthquakes = [[1,0,38400],
               [2,38601,72000],
               [3,82001,120000],
               [4,120001,153000],
               [5,180001,227000],
               [6,228001,287600],
               [7,312001,345500],
               [8,372001,419000],
               [9,444001,503000]]

models = ['1','2','3','4a','4b','4c','4d','5a','5b','5c','5d']
        
#%%
for model in models:
    # Reading displacement results
    fname = 'Model%s-Results/model%s-DFree.out'%(model,model)
    print ('Working in file %s'%(fname))
    data = np.genfromtxt (fname, delimiter="")
    for earthquake in earthquakes:
        print ('Working on EQ%s'%(earthquake[0]))
        plt.plot(data[earthquake[1]:earthquake[2],0],data[earthquake[1]:earthquake[2],1])
        plt.xlabel('Time (s)')
        plt.ylabel ('EQ%s Displacement (in)'%(earthquake[0]))
        font = {'weight' : 'bold','size' : 26}   
        plt.savefig('Displacement_Result_model%s_EQ%s.pdf'%(model,earthquake[0]), bbox_inches='tight')
        plt.close("all")

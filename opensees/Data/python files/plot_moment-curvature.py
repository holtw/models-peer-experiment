 # -*- coding: utf-8 -*-
# """
# This file plots the reactions of the column
# """
 
#%% Import libraries
import numpy as np
import matplotlib.pyplot as plt
 


fname = 'moment-curvature.csv'
mcurv = np.loadtxt(fname, delimiter = ',')
print ('Working in file ')
plt.close("all")
plt.plot(mcurv[:,0],mcurv[:,1])
plt.xlabel('Curvature (rad/in)')
plt.ylabel ('Moment (in-kip)')
plt.xlim([0,0.0008])
plt.ylim([0,40000])
plt.grid()
font = {'weight' : 'bold','size' : 26}   
plt.savefig('moment_curvature_for_model2.pdf', bbox_inches='tight')
plt.close("all")
    


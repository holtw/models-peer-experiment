# -*- coding: utf-8 -*-
"""
This file plots the displacement of the column
"""

#%% Import libraries
import numpy as np
import matplotlib.pyplot as plt

#%% Read displacement file
displacement = np.loadtxt('model2-DFree.out',delimiter = ' ', skiprows = 10)

#%% Plot results
plt.plot(displacement[:,0],displacement[:,1])
plt.xlabel('Time (s)')
plt.ylabel ('Displacement (in)')

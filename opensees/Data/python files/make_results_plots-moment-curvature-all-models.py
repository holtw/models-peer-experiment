# -*- coding: utf-8 -*-
"""
Created on Thu Sep 21 10:16:08 2017

@author: Wes
"""

#%% Importing libraries
import numpy as np
import matplotlib.pyplot as plt

#%% Constants
earthquakes = [[10,36000],
               [36001,72000],
               [72001,120000],
               [120001,168000],
               [168001,228000],
               [228001,288000],
               [288001,360000],
               [360001,432000],
               [432001,504000]]

models = ['3','4a','4b','4c','4d','5a','5b','5c','5d']
earthquake_ids = ['1','2','3','4','5','6','7','8','9']

# models = ['3']
# earthquake_ids = ['3']

curvature_ranges = [0.075, 0.075, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30, 0.30]


# earthquake_labels = ['EQ1 Loma Prieta \n Agnew State \n Hospital Station ','EQ2 Loma Prieta \n Corralitos Station','EQ3 Loma Prieta \n LGPC Station','EQ4 Loma Prieta \n Corralitos Station','EQ5 Kobe \n Scale: -0.8','EQ6 Loma Prieta \n LGPC Station','EQ7 Kobe \n Scale: 1.0','EQ8 Kobe \n Scale: -1.2','EQ9 Kobe \n Scale: 1.2']


#%%
dresults = list()
for model in models:
    # Reading curvature results
    fname = 'Model%s-Results/model%s-DefoColSec1.out'%(model,model)
    print ('Working in file %s'%(fname))
    datac = np.genfromtxt (fname, delimiter="")
    # Reading moment results
    fname = 'Model%s-Results/model%s-RBase.out'%(model,model)
    datam = np.genfromtxt (fname, delimiter="")
        
    
    for earthquake, earthquake_id, curvature_range in zip(earthquakes, earthquake_ids, curvature_ranges):
        curvatures = datac[earthquake[0]:earthquake[1],2]
        moments = datam[earthquake[0]:earthquake[1],3]

        # Creating the plots
        plt.close('all')
        plt.plot(curvatures,moments*-1)
        plt.plot(curvatures[0], moments[0], 'ro', markersize=7)
        plt.plot(curvatures[-1], moments[-1], 'r<', markersize=7)
        plt.ylabel ('Base Moment (in-kips)')
        plt.xlabel ('Curvature (rad/in)')
        plt.xlim(curvature_range/-39.37, curvature_range/39.37)
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
        plt.ylim(-70806,70806)   
        ax2 = plt.twinx()
        ax2.set_ylabel ('Base Moment (kN-m)')
        ax2.set_ylim(-8000,8000)
        ax3 = plt.twiny()
        ax3.set_xlabel ('Curvature (rad/m)')
        ax3.set_xlim(curvature_range*-1,curvature_range*1)
        plt.grid(True)
        ax2.yaxis.grid(True)
        plt.tight_layout()
        plt.savefig('Moment_Curvature_eq%s_m%s.pdf'%(earthquake_id, model), bbox_inches='tight')

        


# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 10:52:41 2017

@author: caicedo
"""

#%% Importing packages
import numpy as np
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

#%% Constants
confidence = np.array([25., 25., 25., 25.])
tmp = sum(confidence)
confidence = confidence/np.sum(confidence)
choices = [[i,j] for i,j in zip(range(4),confidence)]


means_shear = np.array([[103, 103, 92, 103],
                        [105, 106, 100, 108],
                        [106, 104, 101, 106],
                        [82, 87, 77, 79],
                        [100, 99, 100, 101],
                        [95, 95, 98, 97],
                        [99, 99, 99, 99],
                        [100, 99, 96, 100],
                        [99, 99, 98, 99]])                        

                        
std_shear = np.array([[2, 2, 2, 2],  
                        [2, 2, 2, 2],
                        [2, 2, 2, 2],
                        [2, 2, 2, 2],
                        [2, 2, 2, 2],
                        [2, 2, 2, 2],
                        [2, 2, 2, 2],
                        [2, 2, 2, 2],
                        [2, 2, 2, 2]])

exp_shear = np.array([112., 157., 200., 90., 182., 173., 183., 167., 170.])
#%%
def weighted_choice(choices = choices):
   total = sum(w for c, w in choices)
   r = np.random.uniform(0, total)
   upto = 0
   for c, w in choices:
      if upto + w >= r:
         return c
      upto += w
   assert False, "Shouldn't get here"
               
#%% Draw random samples
d = {}
for eq_number in range(9):
    samples = []
    for i in range(100000):
        # Select the model
        model = weighted_choice()
        rnum = np.random.normal(means_shear[eq_number,model],std_shear[eq_number,model])
        samples.append(rnum)
    samples = np.array(samples)
    d['Eq %i'%(eq_number+1)] = samples

#%% Making the dataframe
df = pd.DataFrame(d)

#%% Making the plot
plt.close('all')
sns.violinplot(data = df)
plt.ylabel('Shear Force (kip)')
plt.ylim([40,140])
plt.plot(range(len(exp_shear)),exp_shear,'ro')
plt.savefig('Statistical_Fx-model4-tight_range-low_stdev.pdf', bbox_inches='tight')
# plt.close('all')
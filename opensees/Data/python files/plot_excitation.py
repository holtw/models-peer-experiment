 # -*- coding: utf-8 -*-
# """
# This file plots the reactions of the column
# """
 
#%% Import libraries
import numpy as np
import matplotlib.pyplot as plt
 

# eqs = ['1','2','3','4','5','6','7','8','9'] 
eqs = ['2']

for eq in eqs:
    fname = 'EQ%s_time_history.csv'%eq
    excitation = np.loadtxt(fname, delimiter = ',')
    print ('Working in file %s'%(fname))
    plt.plot(excitation[:,0],excitation[:,1])
    plt.xlabel('Time (s)')
    plt.ylabel ('EQ%s Acceleration (g)'%(eq))
    plt.xlim([0,120])
    plt.ylim([-1.0,1.0])
    font = {'weight' : 'bold','size' : 26}   
 #   plt.savefig('Excitation Record_EQ%s.pdf'%(eq), bbox_inches='tight')
 #   plt.close("all")
    


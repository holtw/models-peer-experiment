# -*- coding: utf-8 -*-
"""
This file plots the reactions of the column
"""

#%% Import libraries
import numpy as np
import matplotlib.pyplot as plt


#%% Read reaction file
displacement = np.loadtxt('RBase.out',delimiter = ' ', skiprows = 10)

#%% Plot results
plt.plot(displacement[:,0],displacement[:,3])
plt.xlabel('Time (s)')
plt.ylabel ('Reaction Mz (in-kip)')

font = {'weight' : 'bold',
        'size' : 26}
#matplotlib.rc('font', **font)        
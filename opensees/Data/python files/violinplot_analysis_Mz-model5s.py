# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 10:52:41 2017

@author: caicedo
"""

#%% Importing packages
import numpy as np
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

#%% Constants
confidence = np.array([25., 25., 25., 25.])
tmp = sum(confidence)
confidence = confidence/np.sum(confidence)
choices = [[i,j] for i,j in zip(range(4),confidence)]


means_moment = np.array([[27940., 32193., 24553., 25890.],
                         [37387., 37192., 34276., 37546.],
                         [34811., 35621., 33236., 36363.],
                         [30710., 30572., 29678., 30023.],
                         [35395., 35473., 33250., 32118.],
                         [31612., 31286., 30690., 30830.],
                         [34137., 32689., 31568., 35630.],
                         [35355., 33199., 34945., 32897.],
                         [34928., 32709., 31892., 32338.]])

std_moment = np.array([ [2000, 2000, 2000, 2000,],
                        [2000, 2000, 2000, 2000,],
                        [2000, 2000, 2000, 2000,],
                        [2000, 2000, 2000, 2000,],
                        [2000, 2000, 2000, 2000,],
                        [2000, 2000, 2000, 2000,],
                        [2000, 2000, 2000, 2000,],
                        [2000, 2000, 2000, 2000,],
                        [2000, 2000, 2000, 2000,]])


exp_moment = np.array([34940., 52180., 58357., 33143., 64083., 57525., 65410., 63401., 54472.])
#%%
def weighted_choice(choices = choices):
   total = sum(w for c, w in choices)
   r = np.random.uniform(0, total)
   upto = 0
   for c, w in choices:
      if upto + w >= r:
         return c
      upto += w
   assert False, "Shouldn't get here"
               
#%% Draw random samples
d = {}
for eq_number in range(9):
    samples = []
    for i in range(100000):
        # Select the model
        model = weighted_choice()
        rnum = np.random.normal(means_moment[eq_number,model],std_moment[eq_number,model])
        samples.append(rnum)
    samples = np.array(samples)
    d['Eq %i'%(eq_number+1)] = samples

#%% Making the dataframe
df = pd.DataFrame(d)

#%% Making the plot
plt.close('all')
sns.violinplot(data = df)
plt.ylabel('Moment (in-kip)')
plt.plot(range(len(exp_moment)),exp_moment,'ro')
plt.ylim(0, 70000)
plt.savefig('Statistical_Mz-model5.pdf', bbox_inches='tight')
plt.close('all')
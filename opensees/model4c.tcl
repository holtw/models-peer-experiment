# --------------------------------------------------------------------------------------------------
# Model 4
# Fiber Element 
# 
# EQ ground motion with gravity
#
# all units are in kip, inch, second
#		
#
#    ^Y
#    |
#    2       __ 
#    |         | 
#    |         | 
#    |         | 
#  (1)      24'
#    |         | 
#    |         | 
#    |         | 
#  =1=    ----  -------->X
#

# Dynamic Input Shortcut
set inputfile "AFSE_ALL_EQ.tcl";
set time 2200;	

# SET UP ----------------------------------------------------------------------------
wipe;					        # clear opensees model
model basic -ndm 2 -ndf 3;	       		# 2 dimensions, 3 dof per node
file mkdir data; 				# create data directory

set G 386;

# define GEOMETRY -------------------------------------------------------------
# nodal coordinates:
node 1 0. 0.;					   # node#, X Y
node 2 0. 288.

# Single point constraints -- Boundary Conditions
fix 1 1 1 1; 				# node DX DY RZ

# nodal masses:
mass 2 1.35 1.35 0.;			   # node#, Mx My Mz, Mass=Weight/g.

# define geometry parameters
set ACol 1810;				# cross-sectional area
set IzCol 97557; 			# Column cracked moment of inertia 

# Define ELEMENTS & SECTIONS  -------------------------------------------------------------
set ColMatTagFlex 2;				# assign a tag number to the column flexural behavior
set ColMatTagAxial 3;				# assign a tag number to the column axial behavior	
set ColSecTag 1;				# assign a tag number to the column section tag
set BeamSecTag 2;				# assign a tag number to the beam section tag
set rebarMatTag 4;
set InteriorConcMatTag 5;
set ExteriorConcMatTag 6;
set fiberSecTag 2

# MATERIAL parameters
set fc -6.; 					# CONCRETE Compressive 
set Ec 3317; 					# Concrete Elastic Modulus


# COLUMN section
uniaxialMaterial ReinforcingSteel $rebarMatTag 75.2 102.4 28426 800 1.1 12.2
uniaxialMaterial Concrete01 $InteriorConcMatTag $fc 0.0026 0.0 0.005
uniaxialMaterial Concrete03 $ExteriorConcMatTag $fc 0.0026 0.0 0.005 0.25 0.580 0.00066 0.193 -1 0.0021

 # FIBER definitions
section Fiber $fiberSecTag {
layer circ $rebarMatTag 18 1.58 0.0 0.0 20.6;
patch circ $InteriorConcMatTag 20 10 0.0 0.0 0.0 20.6 0.0 360.0;
patch circ $ExteriorConcMatTag 20 2 0.0 0.0 20.6 24 0.0 360.0;
}

# define geometric transformation: performs a linear geometric transformation of beam stiffness and resisting force from the basic system to the global-coordinate system
set ColTransfTag 1; 				# associate a tag to column transformation
geomTransf Linear $ColTransfTag  ; 	

# element connectivity:
set numIntgrPts 6;								        # number of integration points for force-based element
element nonlinearBeamColumn 1 1 2 $numIntgrPts $fiberSecTag $ColTransfTag -mass 0.00043;	# self-explanatory when using variables


# Define RECORDERS -------------------------------------------------------------
recorder Node    -file Data/Model4c-Results/model4c-DFree.out -time -node 2 -dof 1 2 3 disp;		# displacements of free nodes
recorder Node    -file Data/Model4c-Results/model4c-DBase.out -time -node 1 -dof 1 2 3 disp;		# displacements of support nodes
recorder Node    -file Data/Model4c-Results/model4c-RBase.out -time -node 1 -dof 1 2 3 reaction;		# support reaction
recorder Drift   -file Data/Model4c-Results/model4c-Drift.out -time -iNode 1 -jNode 2 -dof 1   -perpDirn 2 ;	# lateral drift
recorder Element -file Data/Model4c-Results/model4c-FCol.out -time -ele 2 globalForce;						# element forces -- column
recorder Element -file Data/Model4c-Results/model4c-ForceColSec1.out -time -ele 1 section 1 force;				# Column section forces, axial and moment, node i
recorder Element -file Data/Model4c-Results/model4c-DefoColSec1.out -time -ele 1 section 1 deformation;				# section deformations, axial and curvature, node i
recorder Element -file Data/Model4c-Results/model4c-ForceColSec$numIntgrPts.out -time -ele 1 section $numIntgrPts force;		# section forces, axial and moment, node j
recorder Element -file Data/Model4c-Results/model4c-DefoColSec$numIntgrPts.out -time -ele 1 section $numIntgrPts deformation;		# section deformations, axial and curvature, node j


# define STATIC LOAD-------------------------------------------------------------
timeSeries Linear 1
pattern Plain 1 1 {
   load 2 0. 0. 0.;				# node#, FX FY MZ --  applied static load
}
constraints Plain;     				# how it handles boundary conditions
numberer Plain;					# renumber dof's to minimize band-width (optimization), if you want to
system BandGeneral;				# how to store and solve the system of equations in the analysis
algorithm Linear;         		       	# use Linear algorithm for linear analysis
integrator LoadControl 0.1;			# determine the next time step for an analysis, # apply load in 10 steps
analysis Static					# define type of analysis static or transient
analyze 10;					# perform analysis
loadConst -time 0.0;				# hold load constant and restart time

# Gravity in the vertical direction
timeSeries Path 3 -dt 0.004167 -filePath vert_accel.tcl -factor $G;
pattern UniformExcitation 3 2 -accel 3;


# DYNAMIC EQ ANALYSIS --------------------------------------------------------
# Uniform Earthquake ground motion (uniform acceleration input at all support nodes)
set GMdirection 1;				# ground-motion direction
set GMfact 1.;				# ground-motion scaling factor

# set up ground-motion-analysis parameters
set DtAnalysis	[expr 0.004167];	# time-step Dt for lateral analysis
set TmaxAnalysis $time;  		# maximum duration of ground-motion analysis

# DYNAMIC ANALYSIS PARAMETERS
# CONSTRAINTS handler -- Determines how the constraint equations are enforced in the analysis (http://opensees.berkeley.edu/OpenSees/manuals/usermanual/617.htm)
#          Plain Constraints -- Removes constrained degrees of freedom from the system of equations 
#          Lagrange Multipliers -- Uses the method of Lagrange multipliers to enforce constraints 
#          Penalty Method -- Uses penalty numbers to enforce constraints 
#          Transformation Method -- Performs a condensation of constrained degrees of freedom 
constraints Transformation ; 

# DOF NUMBERER (number the degrees of freedom in the domain): (http://opensees.berkeley.edu/OpenSees/manuals/usermanual/366.htm)
#   determines the mapping between equation numbers and degrees-of-freedom
#          Plain -- Uses the numbering provided by the user 
#          RCM -- Renumbers the DOF to minimize the matrix band-width using the Reverse Cuthill-McKee algorithm 
numberer Plain

# SYSTEM (http://opensees.berkeley.edu/OpenSees/manuals/usermanual/371.htm)
#   Linear Equation Solvers (how to store and solve the system of equations in the analysis)
#   -- provide the solution of the linear system of equations Ku = P. Each solver is tailored to a specific matrix topology. 
#          ProfileSPD -- Direct profile solver for symmetric positive definite matrices 
#          BandGeneral -- Direct solver for banded unsymmetric matrices 
#          BandSPD -- Direct solver for banded symmetric positive definite matrices 
#          SparseGeneral -- Direct solver for unsymmetric sparse matrices (-piv option)
#          SparseSPD -- Direct solver for symmetric sparse matrices 
#          UmfPack -- Direct UmfPack solver for unsymmetric matrices 
system SparseGeneral -piv

# TEST: # convergence test to 
# Convergence TEST (http://opensees.berkeley.edu/OpenSees/manuals/usermanual/360.htm)
#   -- Accept the current state of the domain as being on the converged solution path 
#   -- determine if convergence has been achieved at the end of an iteration step
#          NormUnbalance -- Specifies a tolerance on the norm of the unbalanced load at the current iteration 
#          NormDispIncr -- Specifies a tolerance on the norm of the displacement increments at the current iteration 
#          EnergyIncr-- Specifies a tolerance on the inner product of the unbalanced load and displacement increments at the current iteration 
#          RelativeNormUnbalance --
#          RelativeNormDispIncr --
#          RelativeEnergyIncr --
set Tol 1.e-8;                        # Convergence Test: tolerance
set maxNumIter 100;                # Convergence Test: maximum number of iterations that will be performed before "failure to converge" is returned
set printFlag 0;                # Convergence Test: flag used to print information on convergence (optional)        # 1: print information on each step; 
set TestType EnergyIncr;	# Convergence-test type
test $TestType $Tol $maxNumIter $printFlag;

# Solution ALGORITHM: -- Iterate from the last time step to the current (http://opensees.berkeley.edu/OpenSees/manuals/usermanual/682.htm)
#          Linear -- Uses the solution at the first iteration and continues 
#          Newton -- Uses the tangent at the current iteration to iterate to convergence 
#          ModifiedNewton -- Uses the tangent at the first iteration to iterate to convergence 
#          NewtonLineSearch -- 
#          KrylovNewton -- 
#          BFGS -- 
#          Broyden -- 
set algorithmType ModifiedNewton 
algorithm $algorithmType;        

# Static INTEGRATOR: -- determine the next time step for an analysis  (http://opensees.berkeley.edu/OpenSees/manuals/usermanual/689.htm)
#          LoadControl -- Specifies the incremental load factor to be applied to the loads in the domain 
#          DisplacementControl -- Specifies the incremental displacement at a specified DOF in the domain 
#          Minimum Unbalanced Displacement Norm -- Specifies the incremental load factor such that the residual displacement norm in minimized 
#          Arc Length -- Specifies the incremental arc-length of the load-displacement path 
# Transient INTEGRATOR: -- determine the next time step for an analysis including inertial effects 
#          Newmark -- The two parameter time-stepping method developed by Newmark 
#          HHT -- The three parameter Hilbert-Hughes-Taylor time-stepping method 
#          Central Difference -- Approximates velocity and acceleration by centered finite differences of displacement 
set NewmarkGamma 0.5;	# Newmark-integrator gamma parameter (also HHT)
set NewmarkBeta 0.25;	# Newmark-integrator beta parameter
integrator Newmark $NewmarkGamma $NewmarkBeta 

# ANALYSIS  -- defines what type of analysis is to be performed (http://opensees.berkeley.edu/OpenSees/manuals/usermanual/324.htm)
#          Static Analysis -- solves the KU=R problem, without the mass or damping matrices. 
#          Transient Analysis -- solves the time-dependent analysis. The time step in this type of analysis is constant. The time step in the output is also constant. 
#          variableTransient Analysis -- performs the same analysis type as the Transient Analysis object. The time step, however, is variable. This method is used when 
#                 there are convergence problems with the Transient Analysis object at a peak or when the time step is too small. The time step in the output is also variable.
analysis Transient

# define DAMPING--------------------------------------------------------------------------------------
# apply Rayleigh DAMPING from $xDamp
# D=$alphaM*M + $betaKcurr*Kcurrent + $betaKcomm*KlastCommit + $beatKinit*$Kinitial
set xDamp 0.02;				# 2% damping ratio
set lambda [eigen 1]; 			# eigenvalue mode 1
set omega [expr pow($lambda,0.5)];
set alphaM 0.;				# M-prop. damping; D = alphaM*M
set betaKcurr 0.;         			# K-proportional damping;      +beatKcurr*KCurrent
set betaKcomm [expr 2.*$xDamp/($omega)];   	# K-prop. damping parameter;   +betaKcomm*KlastCommitt
set betaKinit 0.;         			# initial-stiffness proportional damping      +beatKinit*Kini
# define damping
rayleigh $alphaM $betaKcurr $betaKinit $betaKcomm; 				# RAYLEIGH damping


# DYNAMIC ground-motion analysis -------------------------------------------------------------
# create load pattern
set G 386
timeSeries Path 2 -dt 0.004167 -filePath $inputfile -factor $G;  # define acceleration vector from file (dt=0.004167 is associated with the input file gm)
pattern UniformExcitation 2 1 -accel 2;		   		   # define where and how (pattern tag, dof) acceleration is applied



set Nsteps [expr int($TmaxAnalysis/$DtAnalysis)];
set ok [analyze $Nsteps $DtAnalysis];			# actually perform analysis; returns ok=0 if analysis was successful

if {$ok != 0} {      ;					# if analysis was not successful.
	# change some analysis parameters to achieve convergence
	# performance is slower inside this loop
	#    Time-controlled analysis
	set ok 0;
	set controlTime [getTime];
	while {$controlTime < $TmaxAnalysis && $ok == 0} {
		set ok [analyze 1 $DtAnalysis]
		set controlTime [getTime]
		set ok [analyze 1 $DtAnalysis]
		if {$ok != 0} {
			puts "Trying Newton with Initial Tangent .."
			test NormDispIncr   $Tol 1000  0
			algorithm Newton -initial
			set ok [analyze 1 $DtAnalysis]
			test $TestType $Tol $maxNumIter  0
			algorithm $algorithmType
		}
		if {$ok != 0} {
			puts "Trying Broyden .."
			algorithm Broyden 8
			set ok [analyze 1 $DtAnalysis]
			algorithm $algorithmType
		}
		if {$ok != 0} {
			puts "Trying NewtonWithLineSearch .."
			algorithm NewtonLineSearch .8
			set ok [analyze 1 $DtAnalysis]
			algorithm $algorithmType
		}
	}
};      # end if ok !0



puts "Done. End Time: [getTime]"

wipe

